<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="http://intranet.grupompe.com.br/public/assets/css/main.css">
    <script src="http://intranet.grupompe.com.br/public/assets/js/vendor/modernizr.js"></script>
    <link rel="shortcut icon" type="image/x-icon" href="http://intranet.grupompe.com.br/public/assets/icons/favicon.ico">
    <style>

      .bar {
        background-color: #5C9BB7;

        background-image:-webkit-linear-gradient(top, #5c9bb7, #5392ad);
        background-image:-moz-linear-gradient(top, #5c9bb7, #5392ad);
        background-image:linear-gradient(top, #5c9bb7, #5392ad);

        box-shadow: 0 1px 1px #CCC;
        border-radius: 2px;
        width: 580px;
        padding: 10px;
        margin: 80px auto 25px;
        position: relative;
        text-align: right;
        line-height: 1;
      }

      .bar a {
        background: #4987A1 center center no-repeat;
        width: 32px;
        height: 32px;
        display: inline-block;
        text-decoration: none !important;
        margin-right: 5px;
        border-right: 2px;
      }

      .bar a.active {
        background-color: #C14694;
      }

      .bar a.list-icon {
        background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOkYzNkFCQ0ZBMTBCRTExRTM5NDk4RDFEM0E5RkQ1NEZCIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOkYzNkFCQ0ZCMTBCRTExRTM5NDk4RDFEM0E5RkQ1NEZCIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6RjM2QUJDRjgxMEJFMTFFMzk0OThEMUQzQTlGRDU0RkIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6RjM2QUJDRjkxMEJFMTFFMzk0OThEMUQzQTlGRDU0RkIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7h1bLqAAAAWUlEQVR42mL8////BwYGBn4GCACxBRlIAIxAA/4jaXoPEkMyjJ+A/g9MDJQBRhYg8RFqMwg8RJIUINYLFDmBUi+ADQAF1n8ofk9yIAy6WPg4GgtDMRYAAgwAdLYwLAoIwPgAAAAASUVORK5CYII=);
      }

      .bar a.grid-icon {
        background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjBEQkMyQzE0MTBCRjExRTNBMDlGRTYyOTlBNDdCN0I4IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjBEQkMyQzE1MTBCRjExRTNBMDlGRTYyOTlBNDdCN0I4Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MERCQzJDMTIxMEJGMTFFM0EwOUZFNjI5OUE0N0I3QjgiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MERCQzJDMTMxMEJGMTFFM0EwOUZFNjI5OUE0N0I3QjgiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4MjPshAAAAXklEQVR42mL4////h/8I8B6IGaCYKHFGEMnAwCDIAAHvgZgRyiZKnImBQsACxB+hNoDAQyQ5osQZIT4gH1DsBZABH6AB8x/JaQzEig++WPiII7Rxio/GwmCIBYAAAwAwVIzMp1R0aQAAAABJRU5ErkJggg==);
      }

      .bar input {
        background: #FFF no-repeat 13px 13px;

        border: none;
        width: 100%;
        line-height: 19px;
        padding: 11px 0;

        border-right: 2px;
        box-shadow: 0 2px 8px #C4C4C4 inset;
        text-align: left;
        font-size: 14px;
        font-family: inherit;
        color: #738289;
        font-weight: bold;
        outline: none;
        text-indent: 40px;
      }

      /*-------------------------
                Layour Lista
      --------------------------*/

      ul.list {
        list-style: none;
        width: 500px;
        margin: 0 auto;
        text-align: left;
      }

      ul.list li {
        border-right: 1px solid #DDD;
        padding: 10px;
        overflow: hidden;
      }

      ul.list li img {
        width: 120px;
        height: 120px;
        float: left;
        border: none;
      }

      ul.list li p {
        margin-left: 135px;
        font-weight: bold;
        color: #6E7A7F;
      }

      /*-------------------------
                Layout Grid
      --------------------------*/

      ul.grid {
        list-style: none;
        width: 570px;
        margin: 0 auto;
        text-align: left;
      }

      ul.grid li {
        padding: 2px;
        float: left;
      }

      ul.grid li img {
        width: 280px;
        height: 280px;
        display: block;
        border: none;
      }


    </style>
  </head>
  <body>

    <div class="container">
      <div ng-app="switchableGrid" ng-controller="SwitchableGridController">

        <div class="bar">

          <!-- Estes dois botões mudam o layout, e produz o UL correto a ser mostrado -->

          <a href="#" class="list-icon" ng-class="{ active: layout == 'list' }" ng-click="layout = 'list'"></a>
          <a href="#" class="grid-icon" ng-class="{ active: layout == 'grid'}" ng-click="layout = 'grid'"></a>
        </div>

        <!-- Nós temos dois layouts. Escolhemos um deles para ser mostrado dependendo do "layout" vinculado -->

        <ul ng-show="layout == 'grid'" class="grid">
          <!-- Uma visualização com fotos grandes e sem texto -->
          <li ng-repeat="p in pics">
            <a href="{{ p.link}}" target="_blank"><img ng-src="{{ p.images.low_resolution.url}}"></a>
          </li>
        </ul>

        <ul ng-show="layout == 'list'" class="list">
          <!-- Visualização compacta com pequenas fotos e títulos -->
          <li ng-repeat="p in pics">
            <a href="{{ p.link}}" target="_blank"><img ng-src="{{ p.images.thumbnail.url}}"></a>
            <p>{{ p.caption.text}}</p>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="http://intranet.grupompe.com.br/public/assets/js/vendor/jquery.js"><\/script>')</script>

  <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
  <script src="http://intranet.grupompe.com.br/public/assets/js/vendor/bootstrap.min.js"></script>
  <script src="http://intranet.grupompe.com.br/public/assets/js/vendor/semantic.js"></script>
  <script src="http://intranet.grupompe.com.br/public/assets/js/vendor/cookie.js"></script>
  <script src="http://intranet.grupompe.com.br/public/assets/js/plugins.js"></script>
  <script src="http://intranet.grupompe.com.br/public/assets/js/main.js"></script>

  <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.23/angular.min.js"></script>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular-resource.min.js"></script>
  <script src="script.js"></script>
</body>
</html>