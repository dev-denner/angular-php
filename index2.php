<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="http://intranet.grupompe.com.br/public/assets/css/main.css">
    <script src="http://intranet.grupompe.com.br/public/assets/js/vendor/modernizr.js"></script>
    <link rel="shortcut icon" type="image/x-icon" href="http://intranet.grupompe.com.br/public/assets/icons/favicon.ico">
  </head>
  <body>
    
    <div class="container">
      <div ng-app="instantSearch" ng-controller="InstantSearchController">
        <div class="form-group">
          <label for="exampleInputPassword1">Texto</label>
          <input type="text" class="form-control" ng-model="searchString" placeholder="Digite seu termo de pesquisa">
        </div>
        <ul class="list-group">
          <li ng-repeat="i in items| searchFor:searchString" class="list-group-item">
            <div class="media">
              <a href="{{ i.url}}" class="pull-left">
                <img class="media-object" ng-src="{{ i.image}}" />
              </a>
              <div class="media-body">{{ i.title}}</div>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="http://intranet.grupompe.com.br/public/assets/js/vendor/jquery.js"><\/script>')</script>

  <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
  <script src="http://intranet.grupompe.com.br/public/assets/js/vendor/bootstrap.min.js"></script>
  <script src="http://intranet.grupompe.com.br/public/assets/js/vendor/semantic.js"></script>
  <script src="http://intranet.grupompe.com.br/public/assets/js/vendor/cookie.js"></script>
  <script src="http://intranet.grupompe.com.br/public/assets/js/plugins.js"></script>
  <script src="http://intranet.grupompe.com.br/public/assets/js/main.js"></script>

  <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.23/angular.min.js"></script>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular-resource.min.js"></script>
  <script src="script.js"></script>
</body>
</html>