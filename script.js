// O controlador é uma função regular JavaScript. Ela é chamada
// uma vez quando o AngularJS roda dentro da declaração ng-controller 

function InlineEditorController($scope) {

  // $scope é um objeto especial que faz
  // estas propriedades disponíveis para o View
  // como variáveis. Aqui nós definimos alguns valores padrão:

  $scope.showtooltip = false;
  $scope.value = 'Click aqui para editar!';

  // Algumas funções auxiliares que estarão
  // disponíveis nas declarações angular

  $scope.hideTooltip = function() {

    // Quando um model é alterado, o View vai ser automaticamente
    // atualizado pelo AngularJS. Neste caso irá esconder o tooltip

    $scope.showtooltip = false;
  }

  $scope.toggleTooltip = function(e) {
    e.stopPropagation();
    $scope.showtooltip = !$scope.showtooltip;
  }
}

function OrderFormController($scope) {

  // Defina o modelo de propriedades. O View vai fazer o loop
  // através do array services e gerar um elemento li
  // para cada um dos itens.

  $scope.services = [
    {
      name: 'Desenvolvimento Web',
      price: 3000,
      active: true
    }, {
      name: 'Design',
      price: 2000,
      active: false
    }, {
      name: 'Integração',
      price: 2500,
      active: false
    }, {
      name: 'Treinamento',
      price: 2200,
      active: false
    }
  ];

  $scope.toggleActive = function(s) {
    s.active = !s.active;
  };

  // Método útil para calcular o preço total
  $scope.total = function() {

    var total = 0;

    // Uso o método auxiliar do Angular 'forEach'
    // para iterar o array services

    angular.forEach($scope.services, function(s) {
      if (s.active) {
        total += s.price;
      }
    });

    return total;
  };

}


// Definindo um novo módulo para nossa aplicação
var app = angular.module("instantSearch", []);

// Crie um filtro de pesquisa instantânea

app.filter('searchFor', function() {

  // Todos os filtros devem retornar uma função. O primeiro 
  // parâmetro é um dado que será filtrado, e o segundo é um 
  // argumento que vai ser passado com dois pontos
  // searchFor: searchString

  return function(arr, searchString) {

    if (!searchString) {
      return arr;
    }

    var result = [];

    searchString = searchString.toLowerCase();

    // Usando o útil método forEach para iterar através do array
    angular.forEach(arr, function(item) {

      if (item.title.toLowerCase().indexOf(searchString) !== -1) {
        result.push(item);
      }
    });

    return result;
  };

});

// O Controlador

function InstantSearchController($scope) {

  // O modelo de dados. Estes itens normalmente são requisitados via Ajax,
  // mas aqui estão simplificados. Veja o próximo exemplo para 
  // dicas usando Ajax.

  $scope.items = [
    {
      url: 'http://tutorialzine.com/2013/07/50-must-have-plugins-for-extending-twitter-bootstrap/',
      title: '50 Must-have plugins for extending Twitter Bootstrap',
      image: 'http://cdn.tutorialzine.com/wp-content/uploads/2013/07/featured_4-100x100.jpg'
    },
    {
      url: 'http://tutorialzine.com/2013/08/simple-registration-system-php-mysql/',
      title: 'Making a Super Simple Registration System With PHP and MySQL',
      image: 'http://cdn.tutorialzine.com/wp-content/uploads/2013/08/simple_registration_system-100x100.jpg'
    },
    {
      url: 'http://tutorialzine.com/2013/08/slideout-footer-css/',
      title: 'Create a slide-out footer with this neat z-index trick',
      image: 'http://cdn.tutorialzine.com/wp-content/uploads/2013/08/slide-out-footer-100x100.jpg'
    },
    {
      url: 'http://tutorialzine.com/2013/06/digital-clock/',
      title: 'How to Make a Digital Clock with jQuery and CSS3',
      image: 'http://cdn.tutorialzine.com/wp-content/uploads/2013/06/digital_clock-100x100.jpg'
    },
    {
      url: 'http://tutorialzine.com/2013/05/diagonal-fade-gallery/',
      title: 'Smooth Diagonal Fade Gallery with CSS3 Transitions',
      image: 'http://cdn.tutorialzine.com/wp-content/uploads/2013/05/featured-100x100.jpg'
    },
    {
      url: 'http://tutorialzine.com/2013/05/mini-ajax-file-upload-form/',
      title: 'Mini AJAX File Upload Form',
      image: 'http://cdn.tutorialzine.com/wp-content/uploads/2013/05/ajax-file-upload-form-100x100.jpg'
    },
    {
      url: 'http://tutorialzine.com/2013/04/services-chooser-backbone-js/',
      title: 'Your First Backbone.js App – Service Chooser',
      image: 'http://cdn.tutorialzine.com/wp-content/uploads/2013/04/service_chooser_form-100x100.jpg'
    }
  ];
}


// Defina um novo módulo. Desta vez iremos declarar uma dependência 
// no módulo ngResource, assim poderemos trabalhar com a API do Instagram

var app = angular.module("switchableGrid", ['ngResource']);

// Crie e registre o novo serviço "instagram"
app.factory('instagram', function($resource) {

  return {
    fetchPopular: function(callback) {

      // O módulo ngResource nos dá o service $resource
      // Isto irá trabalhar com AJAX facilmente.
      // Aqui eu estou usando um client_id como um teste app.
      // Troque isto pelo seu

      var api = $resource('https://api.instagram.com/v1/media/popular?client_id=:client_id&callback=JSON_CALLBACK', {
        client_id: '642176ece1e7445e99244cec26f4de1f'
      }, {
        // Isto irá criar uma ação que nós nomeamos "fetch"
        // Isso emite uma requisição JSONP a URL da fonte. 
        // JSONP requer que a parte callback=JSON_CALLBACK
        // seja inclusa na URL.

        fetch: {method: 'JSONP'}
      });

      api.fetch(function(response) {

        // Chame a função callback suplementar
        callback(response.data);
      });
    }
  }
});

// O controlador. Note que eu inclui nosso service instragram que
// nós definimos abaixo. Ele estará disponível dentro da função automaticamente

function SwitchableGridController($scope, instagram) {

  // Define o layout da aplicação. Clicando nos botões da 
  // barra de ferramentas para mudar seus valores

  $scope.layout = 'grid';

  $scope.pics = [];

  // Use o service instragram e busque uma lista das fotos populares
  instagram.fetchPopular(function(data) {

    // Atribuindo ao array pics irá ocorrer que
    // o Angular automaticamente irá redesenhar o View
    $scope.pics = data;
  });

}