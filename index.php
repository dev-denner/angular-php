<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="http://intranet.grupompe.com.br/public/assets/css/main.css">
    <script src="http://intranet.grupompe.com.br/public/assets/js/vendor/modernizr.js"></script>
    <link rel="shortcut icon" type="image/x-icon" href="http://intranet.grupompe.com.br/public/assets/icons/favicon.ico">

    <style>
      .tooltips {
        background-color: #5C9BB7;

        background-image: -webkit-linear-gradient (top, #5C9BB7, #5392AD);
        background-image: -moz-linear-gradient (top, #5C9BB7, #5392AD);
        background-image: linear-gradient (top, #5C9BB7, #5392AD);

        box-shadow: 0 1px 1px #CCC;
        border-radius: 3px;
        width: 290px;
        padding: 10px;

        position: absolute;
        left: 50%;
        margin-left: -150px;
        top: 80px; 
      }

      .tooltips:after {
        content: '';
        position: absolute;
        border: 6px solid #5190AC;
        border-color: #5190AC transparent transparent;
        width: 0;
        height: 0;
        bottom: -12px;
        left: 50%;
        margin-left: -6px;
      }

      .tooltips input {
        border: none;
        width: 100%;
        line-height: 34px;
        border-radius: 3px;
        box-shadow: 0 2px 6px #BBB inset;
        text-align: center;
        font-size: 16px;
        font-family: inherit;
        color: #8D9395;
        font-weight: bold;
        outline: none;
      }

      .p {
        font-size: 22px;
        font-weight: bold;
        color: #6D8088;
        height: 30px;
        cursor: pointer;
      }

      .p b {
        color: #FFF;
        display: inline-block;
        padding: 5px 10px;
        background-color: #C4D7E0;
        border-radius: 2px;
        text-transform: uppercase;
        font-size: 18px;

      }

      .p:before {
        content: '✎';
        font-size: 2em;
        display: inline-block;
        margin-right: 5px;
        font-weight: normal;
        vertical-align: middle;
        transition: 0.25s;
      }

    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!--[if lt IE 7]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->

    <!-- 	O menu de navegação vai pegar o valor da variável "ativa" como uma classe. 
          O $event.preventDefault() previne que a página pule quando um link é clicado 
    -->

    <div class="container">
      <div  ng-app>
        <div class="masthead">
          <h3 class="text-muted">Angular</h3>
          <ul class="nav nav-justified {{ativo}}" ng-click="$event.preventDefault()">
            <li class="active"><a href="#" ng-click="ativo = 'Home'">Home</a></li>
            <li><a href="#" ng-click="ativo = 'Projects'">Projects</a></li>
            <li><a href="#" ng-click="ativo = 'Services'">Services</a></li>
            <li><a href="#" ng-click="ativo = 'Downloads'">Downloads</a></li>
            <li><a href="#" ng-click="ativo = 'About'">About</a></li>
            <li><a href="#" ng-click="ativo = 'Contact'">Contact</a></li>
          </ul>
        </div>

        <!-- Jumbotron -->
        <div class="jumbotron">
          <h1 ng-show="ativo">{{ativo}}</h1>
          <h1 ng-hide="ativo">Oi</h1>

          <p class="lead" ng-show="ativo">Sua Escolha <b>{{ativo}}</b></p>
          <p class="lead" ng-hide="ativo">Por favor, click em um item do menu</p>

        </div>
        <div ng-controller="InlineEditorController">
          <!-- Quando este elemento é clicado, esconde o tooltip -->
          <div id="main" ng-click="hideTooltip()">

            <!-- Este é o tooltip. Ele é mostrado somente quando a variável showtooltip for verdadeira -->
            <div class="tooltips" ng-click="$event.stopPropagation()" ng-show="showtooltip">

              <!-- ng-model liga os conteúdos do campo de texto com o modelo "value". Qualquer mudança no campo de texto vai automaticamente atualizar o valor, e todas as outras ligações da página que dependam disto  -->

              <div class="form-group">
                <label for="exampleInputPassword1">Texto</label>
                <input type="text" class="form-control" ng-model="value" />
              </div>
            </div>
            <!-- Chamando um método definido em InlineEditorController que alterna a variável showtooltip -->
            <p class="p" ng-click="toggleTooltip($event)">{{value}}</p>
          </div>
        </div>
        <div ng-controller="OrderFormController">
          <form class="form-horizontal" role="form">
            <h1>Serviços</h1>
            <ul class="list-group">
              <!-- Percorre o array 'services', atribuindo um manipulador click, e defini ou remove a classe css "active" se necessário -->
              <li ng-repeat="service in services" ng-click="toggleActive(service)" ng-class="{ active:service.active }" class="list-group-item">
                <!-- Note o uso do 'current filter', que vai formatar o preço -->
                {{ service.name}} <span>{{ service.price | currency: "R$ " }}</span>
              </li>
            </ul>
            <div class="total">
              <!-- Calcule o preço total de todas as escolhas de serviços. Formate isso como moeda -->
              Total: <span>{{ total() | currency
                          :"R$ "}}</span>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>


  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="http://intranet.grupompe.com.br/public/assets/js/vendor/jquery.js"><\/script>')</script>

  <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
  <script src="http://intranet.grupompe.com.br/public/assets/js/vendor/bootstrap.min.js"></script>
  <script src="http://intranet.grupompe.com.br/public/assets/js/vendor/semantic.js"></script>
  <script src="http://intranet.grupompe.com.br/public/assets/js/vendor/cookie.js"></script>
  <script src="http://intranet.grupompe.com.br/public/assets/js/plugins.js"></script>
  <script src="http://intranet.grupompe.com.br/public/assets/js/main.js"></script>

  <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.23/angular.min.js"></script>
  <script src="script.js"></script>
</body>
</html>